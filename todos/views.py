from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import ListForm, ItemForm
# Create your views here.

def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "list_detail": list_detail,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
            return redirect("todo_list_detail", id=update.id)
    else:
        form = ListForm(instance=update)
    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context={
        "form": form
    }
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, id):
    updateitem = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=updateitem)
        if form.is_valid():
            updateitem=form.save()
            return redirect("todo_list_detail", id=updateitem.id)
    else:
        form = ItemForm(instance=updateitem)
    context={
        "form": form
    }
    return render(request, "todos/updateitem.html", context)
